<?php

namespace App\Http\Controllers;

use App\Events\CampaignUploadedWithSuccess;
use App\Events\NewMessage;
use App\Models\User;
use Illuminate\Http\Request;
use App\Events\Message;

class SinglePageController extends Controller
{
    public function index() {

//        $user = User::first();
//        event(new CampaignUploadedWithSuccess($user, 'testing a notification'));


        Message::dispatch();
        broadcast(new NewMessage())->toOthers();

        return view('app');
    }
}
